/*
 * Copyright (C) 2016 Naman Dwivedi
 *
 * Licensed under the GNU General Public License v3
 *
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 */

package ygs.treevisualization;

import android.content.Context;


import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class DataUtils {

    public static final int[] bst_array = {5, 8, 10, 3, 1, 6, 9, 7, 2, 0,11};

    public static final int[][] bst = {
            {5, 8, 10, 3, 1, 6, 9, 7, 2, 0,11}, //nodes
            {3, 6, 9, 1, 0, 7, -1, -1, -1, -1,-1}, //left child of nodes
            {8, 10, 9, -1, 2, 7, -1, -1, -1, -1,-1} //right child of nodes
    };




    public static BinarySearchTree createBinaryTree() {
        BinarySearchTree b = new BinarySearchTree();
        for (int i = 0; i < bst_array.length; i++) {
            b.insert(bst_array[i]);
        }
        return b;

    }


    public static int getRandomKeyFromBST() {
        int rnd = new Random().nextInt(bst_array.length);
        return bst_array[rnd];
    }

}
